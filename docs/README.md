# **Portfolio** - Website documentation

This is the documentation for the website of **Portfolio**. Have a look
at the [repository](https://gitlab.com/gzaretti/portfolio) or the
[official website](https://gzaretti.gitlab.io/portfolio/) for more details.

## Designs

These designs should help to think about the website and how to present the
information on it.

The design is inspired of a free tailwindcss theme. 
### Colors

The color palette has been done with [Coolors](https://coolors.co/).
_TODO_
As a primary test,
_TODO_[the following color palette](https://coolors.co/e9e387-f4c190-ff9f98-d1abaf-a2b6c5-a79fa3-ab8781-857c7a-5e7173-383d3b)
has been chosen:
_TODO_
- `#002642`
- `#364156`
- `#840032`
- `#e59500`
- `#e5dada`
- `#02040f`
- `#86bbd8`
- `#33658a`
- `#eeba0b`

## Run the project

### Prerequisites

The following prerequisites must be installed to run this project:

- [Hugo with "extended" Sass/SCSS support](https://gohugo.io/) - The world’s fastest framework for building
  websites.
- [Node.js](https://nodejs.org) - A JavaScript runtime built on Chrome's V8
  JavaScript engine.

### Clone the repository

```sh
# Clone the repository
git clone https://gitlab.com/gzaretti/portfolio.git

# Move to the cloned directory
cd portfolio

# Install the dependencies
npm install

```

### Start the project

```sh
# Start the project
hugo server
```

The live website is then available on
[http://localhost:1313/](http://localhost:1313/).

## Technologies used

### Frameworks and libraries

- [tailwindcss](https://tailwindcss.com/) - A free CSS framework based on
  Flexbox. Rapidly build modern websites without ever leaving your HTML. 
  A utility-first CSS framework packed with classes like flex,
  pt-4, text-center and rotate-90 that can be composed to build any 
  design, directly in your markup.
- [Hugo](https://gohugo.io/) - The world’s fastest framework for building
  websites.
- [Node.js](https://nodejs.org) - A JavaScript runtime built on Chrome's V8
  JavaScript engine.
- [PostCSS](https://postcss.org/) - A tool for transforming CSS with JavaScript.

### Quality, performance and security audit tools

_TODO_

### Tools and plugins

- [Coolors](https://coolors.co/) - Create the perfect palette or get inspired by
  thousands of beautiful color schemes.
- [Visual Studio Code](https://code.visualstudio.com/) - A code editor redefined
  and optimized for building and debugging modern web and cloud applications.
  - [EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig) -
    Override user/workspace settings with settings found in `.editorconfig`
    files.

