---
title: "{{ replace .Name "-" " " | title }}"
description: ""
date: {{ .Date }}
draft: true
type: "page"

banner: "/images/projects-in-progress/project-banner.jpg"

tags: []

menu:
  main:
    parent: Projects in progress
---
