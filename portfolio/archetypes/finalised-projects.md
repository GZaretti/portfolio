---
title: "{{ replace .Name "-" " " | title }}"
description: ""
date: {{ .Date }}
draft: true
type: "page"

banner: "/images/finalised-projects/project-banner.jpg"

tags: []

menu:
  main:
    parent: Finalised projects
---
