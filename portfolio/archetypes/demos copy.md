---
title: "{{ replace .Name "-" " " | title }}"
description: ""
date: {{ .Date }}
draft: true
type: "page"

banner: "/images/banner.jpg"

tags: []

menu:
  main:
    parent: Demos
---