document.addEventListener("DOMContentLoaded", function() {
    var btn = document.querySelector(".mobile-menu-button");
    var menu = document.querySelector(".mobile-menu");
    if(btn){
        btn.addEventListener('click', () => {
            menu.classList.toggle("hidden");
    });   
} 
});