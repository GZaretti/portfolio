---
title: "Projet Cosmos"
description: "Mise en place d'une infrastructure Kubernetes pour déveloper des pipelines CI/CD Cloud-Native pour des applications Microservices"
date: 2021-09-22T12:57:13+02:00
draft: false
type: "page"

tags:
  - "ArgoCD"
  - "Artefact repository manager"
  - "CD"
  - "CI/CD"
  - "Cloud-native"
  - "DevOps"
  - "Docker"
  - "GitOps"
  - "Kubernetes"
  - "kubernetes-native"
  - "Pipelines CI/CD"
  - "Tekton"

menu:
  main:
    parent: Projets réalisés
---
Le projet Cosmos est ....