---
title: "Livraison Continue de Tekton ci/cd avec ArgoCD"
description: "Livraison Continue de Tekton ci/cd avec ArgoCD"
date: 2021-09-22T12:57:13+02:00
draft: false
type: "page"


tags:
  - "ArgoCD"
  - "CD"
  - "CI/CD"
  - "Cloud-native"
  - "DevOps"
  - "GitOps"
  - "Kubernetes"
  - "Tekton"

menu:
  main:
    parent: Projets en cours
---
Livraison continu d'une plateforme de CI / CD cloud native ! 