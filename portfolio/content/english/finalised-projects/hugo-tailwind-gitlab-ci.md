---
title: "Create a static website"
description: "Static website with Hugo, setting up a TailwindCSS theme and use the Continous Delivery on GitLab Pages with GitLab CI !"
date: 2021-09-28T12:57:13+02:00
draft: false
type: "page"


tags:
  - "CI/CD"
  - "CD"
  - "Hugo"
  - "Go"
  - "PostCSS"
  - "Pipelines"
  - "GitLab CI"
  - "GitLab Pages"

banner: "/images/finalised-projects/static-website-banner.png"

menu:
  main:
    parent: Finalised projects
---

This is a static website was created to talk about some fun projects, Proof of Concept and challenges at home. I would like to present the concepts and posts informations about the future projects and finalised projects. I think it's a really good way to comunicate an present a little post about my home projets.   

I have devloped this website with Hugo! because Hugo is the world’s fastest framework for building websites. Hugo is one of the most popular open-source static site generators. I have choosen Hugo because a static website use pratically any ressources for the hosting. Hugo have an amazing speed and flexibility and makes building websites fun again. This static website using this own theme, I have setting up with PostCSS and TailwindCSS. PostCSS is a JavaScript CSS parser that is based on the plugins, PostCSS transform CSS to a JavaScript object. The Nodes.js modules apply the preprocess on the JavaScritp object genereted, add additional syntax and return it under a CSS code. I have choosen TailwindCSS because it'is used for rapidly build modern websites without ever leaving your HTML. A utility-first CSS framework packed with classes that can be composed to build any design, directly in your markup.
For this project I don't need to handle a web server because the website is static and hosted on GitLab pages.
The code source and the content is hosted on GitLab (open source distributed version control system). I have used DevOps key concept for the Continous Integration, Continous Deployment and Continous Delivery of this blog. The static website is genereted automatically with GitLab CI and hosted on GitLab Pages. The CI/CD pipeline start when you commit the file on the main branch. If I reach the GitLab quotas for GitLab runner, I have set up the GitLab Runner on a local k8s cluster, the gitlab can runs the CI/CD jobs in a k8s deployment runner.

The source code and the content is hosted on [GitLab](https://gitlab.com/GZaretti/Portfolio).
The website is hosted on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
The technologies used :
  - Hugo
  - Tailwind CSS
  - PostCSS
  - GitLab CI
  - GitLab Runner (on k8s cluster)


