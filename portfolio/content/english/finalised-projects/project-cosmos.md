---
title: "Project Cosmos"
description: "Set up a Kubernetes infrastructure and develope the automated Cloud Native CI/CD pipelines for Microservices"
date: 2021-09-22T12:57:13+02:00
draft: false
type: "page"


tags:
  - "ArgoCD"
  - "Artefact repository manager"
  - "CD"
  - "CI/CD"
  - "Cloud-native"
  - "DevOps"
  - "Docker"
  - "GitOps"
  - "Kubernetes"
  - "kubernetes-native"
  - "Pipelines CI/CD"
  - "Tekton"

banner: "/images/finalised-projects/cosmos-project.png"

menu:
  main:
    parent: Finalised projects
---
The project Cosmos is ....